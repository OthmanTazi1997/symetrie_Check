def matriceSymetriqueCheckR(matrice):
        
    for i in range (0,len(matrice[0])):
        for j in range (i+1,len(matrice[0])):
            #print("check if matrix[" , i , "][" , j , "] == matrix[" , j , "][" , i , "]")
            #print(matrice[i][j] , "__________" , matrice[j][i])
            if matrice[i][j]!= matrice[j][i]:
                #print(matrice[i][j] , "__________" , matrice[j][i])                       
                return False
    return  True 


def matriceSymetriqueCheckL2(matrice):
    
    for j in range(0,len(matrice)):
        for i in range(0,len(matrice)-(j+1)):
            #print("check if matrix[" , i , "][" , j , "] == matrix[" , (len(matrice))-(j+1) , "][" , (len(matrice))-(i+1) , "]") 
            #print(matrice[i][j] , "__________" ,  matrice[(len(matrice))-(j+1)][(len(matrice))-(i+1)])
            if matrice[i][j] != matrice[(len(matrice))-(j+1)][(len(matrice))-(i+1)] :
                #print(matrice[i][j] , "__________" ,  matrice[(len(matrice))-(j+1)][(len(matrice))-(i+1)])
                return False
    return True


def matriceSymetriqueCheckL(matrice):
    symL = True
    check1=""
    check2=""
    for i in range (0,len(matrice[0])):
        for j in range (0,len(matrice[0])-(i+1)):
            check1 +=  str(matrice[i][j])
            #print(check1)
            #print ("M", i , j , "=" , matriceB[i][j] )
    #print("_______")
    for j in range (len(matrice[0]),0,-1):
        for i in range (len(matrice[0]),(len(matrice[0])-(j-1)),-1):
            check2 +=  str(matrice[i-1][j-1])
            #print(check2)
            #print ("M", i-1 , j-1 , "=" , matriceB[i-1][j-1] )

    if check1 != check2 :
        symL = False
    
    return symL





def symetrieCheck(matrice):
    return matriceSymetriqueCheckL(matrice) or matriceSymetriqueCheckR(matrice)

def symetrieCheck(matrice):
    return matriceSymetriqueCheckL2(matrice) or matriceSymetriqueCheckR(matrice)
